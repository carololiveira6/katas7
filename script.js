function newForEach(array, functionCallback) {
    for (let i = 0; i < array.length; i++) {
        functionCallback(array[i], i, array)
    }
}

function newMap(array, functionCallbak) {
    newArray = []
    for (let i = 0; i < array.length; i++) {
        newArray.push(functionCallbak(array[i], i, array))
    }
    return newArray
} 

function newSome(array, functionCallback) {
    for (let i = 0; i < array.length; i++) {
        if (functionCallback(array[i], i, array)) 
            return true
    }
    return false
}

function newFind(array, functionCallback) {
    for (let i = 0; i < array.length; i++) {
        if (functionCallback(array[i], i, array)) 
            return array[i] // retorna o elemento
    }
    return undefined
}

function newFindIndex(array, functionCallback) {
    for (let i = 0; i < array.length; i++) {
        if (functionCallback(array[i], i, array)) 
            return i // retorna o índice
    }
    return - 1
}

function newEvery(array, functionCallback) {
    for (let i = 0; i < array.length; i++) {
        if (functionCallback(array[i], i, array)) 
            return true
    }
    return false
}

function newFilter(array, functionCallback) {
    newArray = []
    for (let i = 0; i < array.length; i++) {
        if (functionCallback(array[i], i, array)) {
            newArray.push(array[i])
        }
    }
    return newArray
}

/* ---------- EXTRAS ---------- */

/*
concat()
Array.prototype.concat()
arr.concat(valor1, valor2, ..., valorN)
valorN:
Arrays ou valores para concatenar (unir) ao array retornado.
*/
function newConcat(firstArray, secondArray) {
    let newArray = []
    for (let i = 0; i < firstArray.length; i++) {
        newArray.push(firstArray[i])
    }
    for (let i = 0; i < secondArray.length; i++) {
        newArray.push(secondArray[i])
    }
    return newArray
}

/*
includes()
Array.prototype.includes()
array.includes(searchElement[, fromIndex])
searchElement:
O elemento a buscar
fromIndex (Opcional):
A posição no array de onde a busca pelo searchElement se iniciará. Por padrão, 0.
*/
function newIncludes(array, searchElement, fromIndex) {
    while (fromIndex !== 0 || fromIndex !== undefined) {
        if (array[fromIndex] === searchElement) {
            return true
        } else {
            return false
        }
    }
}

/*
indexOf()
Array.prototype.indexOf()
array.indexOf(elementoDePesquisa, [pontoInicial = 0])
elementoDePesquisa:
Elemento a ser pesquisado no array.
pontoInicial:
O índice para iniciar a procura. Se o índice for maior ou igual ao tamanho do array, é retornado -1 e signfica que o item não será procurado. Se o pontoInicial é fornecido com um número negativo,  é tomado como deslocamento da extremidade do array. Nota: se o pontoInicial fornecido é negativo, a procura no array acontece de frente para trás. Se o pontoInicial fornecido é 0, então o array inteiro será pesquisado. Padrão: 0 (pesquisa em todo array).
*/
function newIndexOf(array, i, pontoInicial){
    if (pontoInicial >= array) {
        return - 1
    } else {
        for (let index = 0; index < array.length; i++) {
            if (array[index] === i) {
                return index
            } else {
                return - 1
            }
        }
    }
}

/*
join()
Array.prototype.join()
arr.join([separador = ','])
separador(Optional):
Específica uma string para separar cada elemento adjacente do array. O separador é convertido em uma string se necessário. Se omitido, os elementos do array são separados com uma vírgula (","). Se o separador for uma string vazia, todos os elementos são juntados sem nenhum caracter entre eles.
Valor de retorno:
Uma string com todos os elementos do array juntos. Se arr.length é 0, uma string vazia é retornada.
*/
function newJoin() {

}


/*
slice()
Array.prototype.slice()
arr.slice([início[,fim]])
início (Optional):
Índice baseado em zero no qual se inicia a extração.
Como um índice negativo, início indica um deslocamento em relação ao fim da sequência. slice(-2) extrai os dois últimos elementos do array.
Se início for omitido, slice inicia a partir do índice 0.
Se início for maior que o comprimento do array, é retornado um array vazio.
fim (Optional):
Índice baseado em zero o qual é o final da extração. slice extrai até, não incluindo, fim.
*/
function newSlice() {

}


/*
flat()
 */
function newFlat() {

}

function newFlatMap() {

}

/*
Array.of()
Array.of(element0[, element1[, ...[, elementN]]])
elementN:
Elementos usados para criar o array.
Valor de retorno:
Uma nova instância de Array. 
 */
function newArrayof() {
    let newArray = []
    for (let prop in arguments) {
        newArray.push(arguments[prop])
    }
    return newArray
}